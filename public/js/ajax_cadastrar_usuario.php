<script>
    function cadastrar_usuario() {
        event.preventDefault()
        let nome = $("#nome").val();
        let usuario = $("#usuario").val();
        let email = $("#email").val();
        let senha = $("#senha").val();
        let confirma_Senha = $("#confirma_Senha").val();

        if(senha == confirma_Senha)
        {

            $.ajax({
                url: 'http://localhost/usuarios/cadastro',
                dataType: "json",
                type: "POST",
                data: {
                    nome: nome,
                    usuario: usuario,
                    email: email,
                    senha: senha,
                    confirma_Senha: confirma_Senha,
                },
                cache: false,
                success: function (data) {
                    if (data == 'ok') {
                        $.toast({
                            heading: 'Success',
                            position:'top-center',
                            text: 'Cadastro realizado com sucesso!',
                            icon: 'success'
                        })
                        window.location.href = "<?php echo $this->url(array('controller' => 'auth', 'action' => 'login'))?>";
                    } else {
                        alert('Não foi possivel realizar o cadastro. Tente novamente mais tarde!')
                    }
                },
                error: function (e) {
                    alert('erro');
                }

            });
        }else{
            $.toast({
                heading: 'Erro',
                position:'top-center',
                text: 'As senhas não conferem!',
                icon: 'error'
            })

        }

        }
</script>