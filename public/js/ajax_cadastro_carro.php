<script src="/js/jquery.mask.js"></script>
<script>
    $(document).ready(function () {
        $('#placa').mask('AAA-0000', {placeholder: "EX. ABC-1234"});
        $('#ano').mask('0000', {placeholder: "EX. 2020"});
        load_marca();
        load_tipo();
    });

    function load_marca() {
        $.ajax({
            url: '<?php echo $this->url(array('controller' => 'Carro', 'action' => 'pega-marca'));?>',
            type: 'get',
            dataType: 'json',
            cache: false,
            success: (data) => {
                const select = data.map((marca) => {
                    return '<option value="' + marca.id + '">' + marca.nome + '</option>'
                })
                $('#marca').append(select)
            },
            error: () => {
                alert('error')
            }
        })

    }

    function load_tipo() {
        $.ajax({
            url: '<?php echo $this->url(array('controller' => 'Carro', 'action' => 'pega-tipo'));?>',
            type: 'get',
            dataType: 'json',
            cache: false,
            success: (data) => {
                console.log(data);
                const select = data.map((tipo) => {
                    return '<option value="' + tipo.id + '">' + tipo.descricao + '</option>'
                })
                $('#tipo').append(select)
            },
            error: () => {
                alert('error')
            }
        })
    }

    function cadastrar_carro() {

        event.preventDefault()
        let modelo = $("#modelo").val();
        let marca = $("#marca").val();
        let tipo = $("#tipo").val();
        let ano = $( "#ano").val()
        let placa = $("#placa").val();


        $.ajax({
            url: "http://localhost/carro/cadastra",
            dataType: "json",
            type: "POST",
            data: {
                modelo: modelo,
                marca: marca,
                tipo: tipo,
                ano: ano,
                placa: placa,
            },
            cache: false,
            success: function (data) {
                if (data=='ok') {
                    $.toast({
                        heading: 'Success',
                        position:'top-center',
                        text: 'Cadastro realizado com sucesso!',
                        icon: 'success'
                    })
                } else {
                    $.toast({
                        heading: 'Erro',
                        position:'top-center',
                        text: 'Não foi possível realizar o cadastro!',
                        icon: 'error'
                    })
                }
            },
            error: function (e) {
                alert('erro');
            }

        });
    }




</script>