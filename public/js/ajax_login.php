<script>
    function logar() {
        event.preventDefault()
        let senha = $('#senha').val()
        let usuario = $('#usuario').val()

        $.ajax({
            url: '<?php echo $this->url(array('controller' => 'usuarios', 'action' => 'pega-usuario'));?>',
            dataType: "json",
            type: "get",
            data: {
                senha: senha,
                usuario: usuario
            },
            cache: false,
            success: function (data) {
                if (data) {
                    window.location.href = "<?php echo $this->url(array('controller' => 'carro', 'action' => 'home'));?>";
                } else {
                    $.toast({
                        heading: 'Erro',
                        text: 'Usuário ou senha inválido',
                        position:'top-center',
                        icon: 'error'
                    })
                }
            },
            error: function (e) {
                alert('erro');
            }
        })
    }
</script>