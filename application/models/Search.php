<?php

class Application_Model_Search extends Zend_Db_Table_Abstract
{
    protected $_name="carros";
    public function buscaPlaca($input)
    {
        $select= $this->select()
            ->from('carros as c', array('c.modelo','c.ano','c.placa'))
            ->join('marca as m', 'c.marca = m.id',  'm.nome')
            ->join('tipo as t', 'c.tipo = t.id', 't.descricao')
            ->where("c.placa like '%$input%'")
            ->setIntegrityCheck(false); // ADD This Line

              return $this->fetchAll($select)->toArray();


    }

}

