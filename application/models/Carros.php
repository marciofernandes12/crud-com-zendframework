<?php

class Application_Model_Carros extends Zend_Db_Table_Abstract
{
    protected $_name = "carros";

    public function cadastrar($dados)
    {
        return $this->insert($dados);
    }

    public function listar()
    {
        $select = $this->select()
            ->from('carros as c', array('c.modelo', 'c.ano', 'c.placa'))
            ->join('marca as m', 'c.marca = m.id', 'm.nome')
            ->join('tipo as t', 'c.tipo = t.id', 't.descricao')
            ->setIntegrityCheck(false); // ADD This Line

        return $this->fetchAll($select)->toArray();
    }

    public function verificarPlaca($placa)
    {
        $select = $this->select()
            ->from('carros', 'id')
            ->where("placa = '$placa'");


        return $this->fetchAll($select)->toArray();
    }


}


