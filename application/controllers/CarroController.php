<?php

class CarroController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
    }

    public function buscarAction()
    {
        $carros = new Application_Model_Carros();
        $lista = $carros->listar();
        $this->view->listaCarros = $lista;
    }

    public function cadastroAction()
    {

    }

    public function homeAction()
    {
    }

    public function pegaMarcaAction()
    {
        $marca = new Application_Model_MarcaCarro();

        $resultado = $marca->BuscaMarca();
        echo json_encode($resultado);

    }

    public function pegaTipoAction()
    {
        $tipo = new Application_Model_Tipo();

        $resultado = $tipo->BuscaTipo();
        echo json_encode($resultado);
    }

    public function pegaPlacaAction()
    {
        $carro = new Application_Model_Search();
        $input = $_POST['input'];
        $resutaldo = $carro->buscaPlaca($input);


        echo json_encode($resutaldo);
    }

    public function cadastraAction()
    {
        if ($this->getRequest()->isPost()) {
            $modelo = $this->getRequest()->getParam("modelo", "");
            $marca = $this->getRequest()->getParam("marca", "");
            $tipo = $this->getRequest()->getParam("tipo", "");
            $ano = $this->getRequest()->getParam("ano", "");
            $placa = $this->getRequest()->getParam("placa", "");

            $data = array(
                'modelo' => $modelo,
                'marca' => $marca,
                'tipo' => $tipo,
                'ano' => $ano,
                'placa' => $placa
            );
            $carros = new Application_Model_Carros();

            if ($carros->verificarPlaca($placa)) {
                echo "<script>alert('Carro já cadastrado!');</script>";
            } else {

                if ($carros->cadastrar($data)) {

                    echo json_encode('ok');

                } else
                    echo json_encode("nok");

            }

        }
    }


}


