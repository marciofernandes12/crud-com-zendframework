<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setName('login');

        $usuario = new Zend_Form_Element_Text('login');
        $usuario->setLabel('Usuário:')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $senha = new Zend_Form_Element_Password('senha');
        $senha->setLabel('Senha:')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Logar')
            ->setAttrib('id', 'submitbutton');

        $this->addElements(array($usuario, $senha, $submit));
    }



}

